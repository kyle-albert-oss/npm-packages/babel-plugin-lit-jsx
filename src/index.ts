/* eslint-disable */
import { PluginObj } from "@babel/core";
import * as types from "@babel/types";
import {
	Expression,
	Identifier,
	JSXAttribute,
	JSXElement,
	JSXEmptyExpression,
	JSXExpressionContainer,
	JSXFragment,
	JSXOpeningElement,
	JSXSpreadAttribute,
	JSXSpreadChild,
	JSXText,
	MemberExpression,
	Node,
} from "@babel/types";
// @ts-ignore
import jsxSyntaxPlugin from "babel-plugin-syntax-jsx";
import { OneOrMany } from "./types";

interface IBabelApi {
	assertVersion(version: number): void;

	types: typeof types;
}

interface IPluginState {
	opts: Readonly<{
		attributeRegExp: string;
		eventRegExp: string;
		propertyRegExp: string;
		matchOrder: string[];
	}>;
}

const DEFAULT_PLUGIN_OPTS: IPluginState["opts"] = {
	attributeRegExp: "(.*)\\$",
	eventRegExp: "on-(.*)",
	matchOrder: ["attribute", "event", "property"],
	propertyRegExp: "(.*)",
};

interface IJSXAttrMatch {
	jsxName?: string;
	attributeName?: string;
	eventName?: string;
	propertyName?: string;
}

type ExpressionsOrStringsArray = OneOrMany<string | Expression | JSXEmptyExpression>[];
type ExpressionsArray = OneOrMany<Expression | JSXEmptyExpression>[];

const flatten = <T>(arrs: T[][]) => arrs.reduce((xs, x) => [...xs, ...x], []);

const voidElements = [
	"area",
	"base",
	"basefont",
	"bgsound",
	"br",
	"col",
	"command",
	"embed",
	"frame",
	"hr",
	"image",
	"img",
	"input",
	"isindex",
	"keygen",
	"link",
	"menuitem",
	"meta",
	"nextid",
	"param",
	"source",
	"track",
	"wbr",
];

export default function plugin(api: IBabelApi): PluginObj {
	api.assertVersion(7);
	const { types: t } = api;

	let opts: IPluginState["opts"];
	let attributeRegExp: RegExp;
	let eventRegExp: RegExp;
	let propertyRegExp: RegExp;

	const pre = (state: IPluginState) => {
		opts = Object.assign({}, DEFAULT_PLUGIN_OPTS, state.opts || {});
		attributeRegExp = new RegExp(`^(?:${opts.attributeRegExp})$`);
		eventRegExp = new RegExp(`^(?:${opts.eventRegExp})$`);
		propertyRegExp = new RegExp(`^(?:${opts.propertyRegExp})$`);
	};

	const matchJsxAttr = (name: string): IJSXAttrMatch => {
		const { matchOrder } = opts;
		const { length } = matchOrder;
		for (let x = 0; x < length; x++) {
			if (matchOrder[x] === "attribute") {
				const match = name.match(attributeRegExp)?.[1];
				if (match) {
					return { attributeName: match };
				}
			} else if (matchOrder[x] === "event") {
				const match = name.match(eventRegExp)?.[1];
				if (match) {
					return { eventName: match };
				}
			} else if (matchOrder[x] === "property") {
				const match = name.match(propertyRegExp)?.[1];
				if (match) {
					return { propertyName: match };
				}
			}
		}
		return {};
	};

	const requireExp = (pkg: string) => t.callExpression(t.identifier("require"), [t.stringLiteral(pkg)]);
	const requireMemberExp = (pkg: string, member: string) => t.parenthesizedExpression(t.memberExpression(requireExp(pkg), t.identifier(member)));

	/**
	 * take array of quasis (strings) + expressions (AST nodes) and produce TemplateLiteral node
	 * @param {Array<*>} parts
	 * @return {object}
	 **/
	function transformElement(parts: ExpressionsOrStringsArray) {
		// we have one mixed array and we need to split nodes by type
		const quasis = [],
			exprs: Expression[] = [];

		let i = 0;
		// do one iteration more to make sure we produce an empty string quasi at the end
		while (i < parts.length + 1) {
			let quasi = "";
			// join adjacent strings into one
			while (typeof parts[i] === "string") {
				// we need to escape backticks and backslashes manually
				quasi += (parts[i] as string).replace(/[\\`]/g, (s: string) => `\\${s}`);
				i += 1;
			}
			quasis.push(t.templateElement({ raw: quasi }));

			// add a single expr node
			if (parts[i] != null) {
				exprs.push((parts as Expression[])[i]);
			}

			i += 1; // repeat
		}

		return t.taggedTemplateExpression(requireMemberExp("lit-html", "html"), t.templateLiteral(quasis, exprs));
	}

	/**
	 * take JSXElement and return array of template strings and parts
	 * @param {*} elem
	 * @return {Array<*>}
	 */
	function renderElement(elem: Node): ExpressionsOrStringsArray {
		if (elem.type == "JSXFragment") {
			const children = elem.children.map(renderChild);
			return [...flatten(children)];
		}
		if (elem.type == "JSXElement") {
			const { tag, isVoid } = renderTag(elem.openingElement.name);
			const attrs = elem.openingElement.attributes.map(renderProp);
			const children = elem.children.map(renderChild);
			return ["<", tag, ...flatten(attrs), ">", ...(isVoid ? [] : flatten(children)), ...(isVoid ? [] : ["</", tag, ">"])];
		}
		throw new Error(`Unknown element type: ${elem.type}`);
	}

	/**
	 * Take JSXElement name (Identifier or MemberExpression) and return JS counterpart
	 * @param {*} name
	 * @param {boolean} root Whether it's the root of expression tree
	 * @return {{tag: *, isVoid: boolean}}
	 */
	function renderTag(name: JSXOpeningElement["name"], root = true): { tag: Identifier | MemberExpression | string; isVoid?: boolean } {
		// name is an identifier
		if (name.type == "JSXIdentifier") {
			const tag = name.name;

			// it's a single lowercase identifier (e.g. `foo`)
			if (root && t.react.isCompatTag(tag)) {
				const isVoid = voidElements.includes(tag.toLowerCase());
				// return it as part of the template (`<foo>`)
				return { tag, isVoid };
			} else if (root) {
				// it's a single uppercase identifier (e.g. `Foo`)
				const object = t.identifier(tag);
				const property = t.identifier("is");
				// imitate React and try to use the class (`<${Foo.is}>`)
				return { tag: t.memberExpression(object, property) };
			} else {
				// it's not the only identifier, it's a part of a member expression
				// return it as identifier
				return { tag: t.identifier(tag) };
			}
		}

		// tag names can also be member expressions (`Foo.Bar`)
		if (name.type == "JSXMemberExpression") {
			const expr = name; // transform recursively
			const { tag: object } = renderTag(expr.object, false);
			const property = t.identifier(expr.property.name);
			const tag = root // stick `.is` to the root member expr
				? t.memberExpression(t.memberExpression(object as Expression, property), t.identifier("is"))
				: t.memberExpression(object as Expression, property);
			return { tag }; // return as member expr
		}

		throw new Error(`Unknown element tag type: ${name.type}`);
	}

	/**
	 * Take JSXAttribute and return array of template strings and parts
	 * @param {*} prop
	 * @return {Array<*>}
	 */
	function renderProp(prop: JSXAttribute | JSXSpreadAttribute): ExpressionsOrStringsArray {
		if (prop.type === "JSXAttribute") {
			const {
				name: { name: innerName },
				value,
			} = prop;
			const jsxName = innerName;
			const { attributeName, eventName, propertyName } = typeof innerName === "string" ? matchJsxAttr(innerName) : ({} as IJSXAttrMatch);

			if (value) {
				if (value.type == "StringLiteral") {
					// value is a string literal
					// we are setting an attribute, no lit-html involved, produce template strings
					if (attributeName) {
						return [" ", `${attributeName}`, "=", value.extra.raw];
					} else if (propertyName) {
						// setting property must involve lit-html, let's create a template expression here
						return [" ", `.${propertyName}`, "=", t.stringLiteral(value.extra.rawValue)];
					} else if (eventName) {
						// setting event handler to a string doesn't make sense
						throw Error(`Event prop can't be a string literal`);
					}
				} else if (value.type == "JSXExpressionContainer" && "expression" in value) {
					// value is an expression
					// modify the name and produce a template expression in all cases
					if (attributeName) {
						let attrExpression = value.expression;
						if (attributeName === "style") {
							// @ts-ignore
							attrExpression = t.callExpression(requireMemberExp("lit-html/directives/style-map", "styleMap"), [attrExpression]);
						} else if (attributeName === "class") {
							// @ts-ignore
							attrExpression = t.callExpression(requireMemberExp("lit-html/directives/class-map", "classMap"), [attrExpression]);
						}
						return [" ", `${attributeName}`, "=", attrExpression];
					} else if (propertyName) {
						return [" ", `.${propertyName}`, "=", value.expression];
					} else if (eventName) {
						return [" ", `@${eventName}`, "=", value.expression];
					}
				}
			} else {
				// we are setting a boolean attribute, no lit-html involved, just remove the `$`
				if (attributeName) {
					return [" ", `${attributeName}`];
				} else if (eventName) {
					// valueless event handler doesn't make sense
					throw Error(`Event prop must have a value`);
				} else if (propertyName) {
					// Valueless property default to `true` (imitate React)
					return [" ", `.${propertyName}`, "=", t.booleanLiteral(true)];
				}
			}

			throw new Error(`Couldn't transform attribute ${JSON.stringify(jsxName)}`);
		} else if (prop.type === "JSXSpreadAttribute") {
			return [" ", "...", "=", t.callExpression(requireMemberExp("@open-wc/lit-helpers", "spread"), [prop.argument])];
		}
		throw new Error(`Couldn't transform attribute ${JSON.stringify(prop)}`);
	}

	/**
	 * Take JSX child node and return array of template strings and parts
	 * @param {*} child
	 * @return {Array<*>}
	 */
	function renderChild(child: JSXText | JSXExpressionContainer | JSXSpreadChild | JSXElement | JSXFragment): ExpressionsOrStringsArray {
		if (child.type == "JSXText") {
			return [(child as any).extra.raw];
		} // text becomes part of template

		if (child.type == "JSXExpressionContainer") {
			if (child.expression.type == "JSXEmptyExpression") {
				return [];
			} else {
				return [child.expression];
			} // expression renders as part
		}

		if (child.type == "JSXElement" || child.type == "JSXFragment") {
			return renderElement(child);
		} // recurse on element

		throw new Error(`Unknown child type: ${child.type}`);
	}

	return {
		inherits: jsxSyntaxPlugin,
		pre,
		visitor: {
			JSXElement(path) {
				path.replaceWith(transformElement(renderElement(path.node)));
			},
			JSXFragment(path) {
				path.replaceWith(transformElement(renderElement(path.node)));
			},
		},
	};
}
