/* eslint no-console: off */

import * as babel from "@babel/core";

const options = { plugins: ["module:./src/index.ts"] };

const transform = (input: string) => babel.transformSync(input, options)!;

const HTML = `(require("lit-html").html)`;
const SPREAD = `(require("@open-wc/lit-helpers").spread)`;
const STYLES = `(require("lit-html/directives/style-map").styleMap)`;
const CLASSES = `(require("lit-html/directives/class-map").classMap)`;

describe("Elements", () => {
	it("Transforms element", () => {
		const input = "<div/>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<div></div>\`;`);
	});

	it("Transforms void element", () => {
		const input = "<br/>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<br>\`;`);
	});

	it("Discards content of void element", () => {
		const input = "<br>test</br>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<br>\`;`);
	});

	it("Transforms fragment", () => {
		const input = "<>foo</>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`foo\`;`);
	});
});

describe("Tag names", () => {
	it("Transforms custom element tag name", () => {
		const input = "<my-element/>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<my-element></my-element>\`;`);
	});

	it("Transforms component class", () => {
		const input = "<Component/>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<$\{Component.is}></$\{Component.is}>\`;`);
	});

	it("Transforms member expression", () => {
		const input = "<foo.bar/>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<$\{foo.bar.is}></$\{foo.bar.is}>\`;`);
	});
});

describe("Attributes", () => {
	it("Transforms string attributes", () => {
		const input = `<input type$="text" value$='foo'/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input type="text" value='foo'>\`;`);
	});

	it("Transforms expression attributes", () => {
		const input = `<input value$={val}/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input value=$\{val}>\`;`);
	});

	it("Doesn't transform entities in string attributes", () => {
		const input = `<input value$="&quot;"/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input value="&quot;">\`;`);
	});

	it("Escapes string attributes", () => {
		const input = `<input value$='\\\\' placeholder$="\`"/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input value='\\\\\\\\' placeholder="\\\`">\`;`);
	});

	it("Transforms boolean attributes", () => {
		const input = `<input disabled$/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input disabled>\`;`);
	});

	it("Transforms string props", () => {
		const input = `<input type="text" value='foo'/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input .type=$\{"text"} .value=$\{"foo"}>\`;`);
	});

	it("Transform entities in string props", () => {
		const input = `<input value="&quot;"/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input .value=$\{"\\""}>\`;`);
	});

	it("Transforms expression props", () => {
		const input = `<input value={val}/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input .value=$\{val}>\`;`);
	});

	it("Transforms boolean props", () => {
		const input = `<input disabled/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input .disabled=$\{true}>\`;`);
	});

	it("Transforms event handlers", () => {
		const input = `<input on-input={console.log}/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input @input=$\{console.log}>\`;`);
	});

	it("Transforms spreads", () => {
		const input = `<input {...myAttrs}/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input ...=$\{${SPREAD}(myAttrs)}>\`;`);
	});

	it("Transforms spreads with expression", () => {
		const input = `<input {...(condition ? myAttrs : myOtherAttrs)}/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input ...=$\{${SPREAD}(condition ? myAttrs : myOtherAttrs)}>\`;`);
	});

	it("Transforms classes expression to use directive", () => {
		const classObjStr = `{ someClass: true, "other-class": false }`;
		const expectedClassStr = `{  someClass: true,  "other-class": false}`;
		const input = `<input class$={${classObjStr}}/>;`;
		const { code } = transform(input);
		expect((code ?? "").replace(/\n/g, "")).toEqual(`${HTML}\`<input class=$\{${CLASSES}(${expectedClassStr})}>\`;`);
	});

	it("Transforms classes string expression without directive", () => {
		const classStr = `my-class other-class`;
		const input = `<input class$="${classStr}"/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input class="${classStr}">\`;`);
	});

	it("Transforms styles expression to use directive", () => {
		const cssObjStr = `{ backgroundColor: "red", "padding-left": 2 }`;
		const expectedCssObjStr = `{  backgroundColor: "red",  "padding-left": 2}`;
		const input = `<input style$={${cssObjStr}}/>;`;
		const { code } = transform(input);
		expect((code ?? "").replace(/\n/g, "")).toEqual(`${HTML}\`<input style=$\{${STYLES}(${expectedCssObjStr})}>\`;`);
	});

	it("Transforms styles string expression without directive", () => {
		const cssStr = `background-color: red, padding-left: 2`;
		const input = `<input style$="${cssStr}"/>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<input style="${cssStr}">\`;`);
	});
});

describe("Children", () => {
	it("Transforms text children", () => {
		const input = `<p> foo bar </p>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<p> foo bar </p>\`;`);
	});

	it("Escapes text children", () => {
		const input = "<p> `\\` </p>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<p> \\\`\\\\\\\` </p>\`;`);
	});

	it("Doesn't transform entities in text children", () => {
		const input = "<p> &quot; </p>;";
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<p> &quot; </p>\`;`);
	});

	it("Transforms expression children", () => {
		const input = `<p> foo: {val} </p>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<p> foo: $\{val} </p>\`;`);
	});

	it("Skips empty expression children", () => {
		const input = `<p>{ } { /* comment */ }</p>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<p> </p>\`;`);
	});

	it("Transforms element children", () => {
		const input = `<p> foo: <b> {val} </b> </p>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<p> foo: <b> $\{val} </b> </p>\`;`);
	});

	it("Transforms fragment children", () => {
		const input = `<p> foo: <>{val}</> </p>;`;
		const { code } = transform(input);
		expect(code).toEqual(`${HTML}\`<p> foo: $\{val} </p>\`;`);
	});
});
