import * as gulp from "gulp";
import { execTask, GulpTask, inlineTask, rmTask } from "./gulp-util";

const { dest, parallel, series, src, task } = gulp;

const srcDir = "src";
const jestBaseCmd = "jest"; // --env=jsdom
const eslintCmd = (opts?: { fix?: boolean }): string => `eslint '${srcDir}/**/*.{js,jsx,ts,tsx}' ${opts && opts.fix ? " --fix " : ""}`;

//
// CLEAN
//

rmTask("clean", (d) => d(["./coverage", "./dist", "./out"]));

//
// LINT
//

execTask("lint", eslintCmd());
execTask("lint:fix", eslintCmd({ fix: true }));

//
// TEST
//

execTask("test:cyclic", `madge --circular --warning --tsConfig ./tsconfig.json --extensions js,jsx,ts,tsx ./${srcDir}`);
execTask("test:lint", eslintCmd());
execTask("test:unit", jestBaseCmd);
execTask("test:unit-with-coverage", `${jestBaseCmd} --coverage`);
const baseTestTasks = ["test:cyclic", "test:lint"];

const getTestTasks = (opts?: { coverage?: boolean }): GulpTask[] => {
	const tasks: string[] = [...baseTestTasks];
	tasks.push(opts && opts.coverage ? "test:unit-with-coverage" : "test:unit");
	return tasks;
};

//
// BUILD
//

const buildTasks = [
	// execTask("build:prod:babel", "babel src --extensions '.ts,.tsx' --out-dir out/babel"),
	// execTask("build:prod:css", "SASS_PATH=node_modules sass src:out/css"),
	execTask("build:prod:rollup", "rollup -c"),
	// inlineTask("build:prod:scss", () => src("./src/**/*.scss").pipe(dest("out/scss"))),
	execTask("build:prod:tsc", "tsc -p ./tsconfig.d.json"),
];

//
// DIST
//

const distTasks = [
	// inlineTask("prepare:dist:copy-babel", () => src("./out/babel/**/*.*").pipe(dest("dist")));
	inlineTask("prepare:dist:copy-declaration", () => src("./out/tsc/**/*.d.ts").pipe(dest("dist"))),
	// inlineTask("prepare:dist:copy-css", () => src("./out/css/**/*.css").pipe(dest("dist/styles/css")));
	// inlineTask("prepare:dist:copy-scss", () => src("./out/scss/**/*.scss").pipe(dest("dist/styles/scss")));
	// rmTask("prepare:dist:delete-babel-index", (d) => d("./dist/index.js")), // HACK: using rollup to generate bundles and babel to compile isolate modules
];

//
// TOP-LEVEL TASKS
//

const getTopLevelBuildTasks = (opts?: { test?: boolean; coverage?: boolean }): GulpTask[] => {
	const tasks: GulpTask[] = ["clean"];
	if (opts && opts.test) {
		tasks.push(opts.coverage ? "build:test-with-coverage" : "build:test");
	}
	tasks.push("build:compile", ...distTasks);
	return tasks;
};

task("test", parallel(...getTestTasks()));
task("build:test", parallel(...getTestTasks()));
task("build:test-with-coverage", parallel(...getTestTasks({ coverage: true })));

task("build:compile", parallel(...buildTasks));
task("build:prod", series(...getTopLevelBuildTasks({ test: true })));

//
// CI TOP-LEVEL TASKS, we want to be as granular as possible to split into pipeline stages
//

task("ci:test", parallel(...getTestTasks({ coverage: true })));
task("ci:build:prod", series(...getTopLevelBuildTasks({ test: false })));
