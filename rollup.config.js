import autoExternal from "rollup-plugin-auto-external";
import nodeResolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import babel from "rollup-plugin-babel";
import { DEFAULT_EXTENSIONS } from "@babel/core";

import pkg from "./package.json";

const extensions = [...DEFAULT_EXTENSIONS, ".ts", ".tsx"];
const entryPoint = "src/index.ts";

const nodeResolveOpts = {
	// builtins: false, // uncomment for browser
	extensions,
};

const babelOpts = {
	exclude: ["node_modules/**"],
	extensions,
	include: ["src/**/*"],
};

// order matters
const sharedPlugins = [
	autoExternal(),
	nodeResolve(nodeResolveOpts), // so Rollup can find `ms`
	commonjs(), // so Rollup can convert `ms` to an ES module
	babel(babelOpts),
];

export default [
	// browser-friendly UMD build
	{
		input: entryPoint,
		output: {
			name: "index",
			file: pkg.browser,
			format: "umd",
		},
		plugins: [...sharedPlugins],
	},
	// CommonJS (for Node) and ES module (for bundlers) build.
	// (We could have three entries in the configuration array
	// instead of two, but it's quicker to generate multiple
	// builds from a single configuration where possible, using
	// an array for the `output` option, where we can specify
	// `file` and `format` for each target)
	{
		input: entryPoint,
		external: ["ms"],
		output: [
			{ file: pkg.main, format: "cjs" },
			{ file: pkg.module, format: "es" },
		],
		plugins: [...sharedPlugins],
	},
];
