// eslint-disable-next-line @typescript-eslint/camelcase
import * as childProcesses from "child_process";
import * as del from "del";
import * as gulp from "gulp";
import * as undertaker from "undertaker";

export type GulpTask = undertaker.Task;

const { task } = gulp;

export const inlineTask = (name: string, fn: gulp.TaskFunction) => {
	task(name, fn);
	return task(name);
};

type DelSync = typeof del.sync;
export const rmTask = (name: string, fn: (d: DelSync) => ReturnType<DelSync>) => {
	return inlineTask(name, async () => {
		// eslint-disable-next-line
		const result = fn(del.sync);
		if (result) {
			result.forEach((path) => console.log(`Deleted ${path}`));
		}
		return Promise.resolve();
	});
};

export const execTaskFn = (cmd: string): gulp.TaskFunction => (cb) => {
	console.log("Executing:", cmd);
	// seems to support dynamic stdio (loading bars, etc)
	const childProcess = childProcesses.spawn(cmd, { stdio: "inherit", shell: true });
	childProcess.on("error", (err) => cb(err));
	childProcess.on("exit", (code) => (code != null && code > 0 ? cb(code) : cb()));
};

export const execTask = (name: string, cmd: string) => inlineTask(name, execTaskFn(cmd));
