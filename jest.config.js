module.exports = {
	preset: "ts-jest",
	testEnvironment: "node",
	moduleFileExtensions: ["js", "jsx", "ts", "tsx"],
	modulePathIgnorePatterns: ["<rootDir>/coverage/", "<rootDir>/dist/", "<rootDir>/out/"],
	//	setupFilesAfterEnv: ["./tests/setup-tests.ts"],
	transform: {
		"\\.(tsx?)$": "ts-jest",
	},
};
